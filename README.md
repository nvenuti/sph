# SPH Shock Tube #

C code for a didactic approach to SPH fluidodynamics and basic OpenMP parallel programming.

Final exam of a class I took in my Masters. The purpose of this code is to test my implementation of SPH dynamics with a 1D shock tube problem.

Pending : 
	- documentation
	- translate comments in english

# Compiling #
The makefile faidoc3.mk handles compilation and creates executable 'doit'

## Running ##

Run 'doit' to evolve the dynamics of the systems (with different options).

Run 'grafico.py' in subdirectory 'outdir' to plot state of the system at various instants.

Author: Nicolò Venuti
