#ifndef MYHEADER_H
#define MYHEADER_H
//PROMEMORIA
//When you use angle brackets, the compiler searches for the file in the include path list.
//When you use MYFLOAT quotes, it first searches the current directory
//(i.e. the directory where the module being compiled is)
//and only then it'll search the include path list.
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <omp.h>
#include <sys/stat.h>
#include <sys/types.h>


//DICHIARAZIONE DELLE VARIABILI E STRUTTURE, E DELLE FUNZIONI USATE IN PIU MODULI

//NNB PUOI USARE FUNZIONI AL POSTO DI VARIABILI GLOBALI O DEFINE
//(LE FUNZIONI SONO SEMPRE extern)


/*
#define epsilon 0.01
#define npoints 3600
#define nsteps 400
*/

//lunghezza tubo (in m)
#define L_SYS 2.0
//massa totale (in kg) (calcolata con volume tot e densità iniz.)
#define M_SYS 1.125
//densità nelle due zone
//(indicativa, usata per decidere come dividere le particelle)
#define RHO_L 1.0
#define RHO_R 0.125
//pressione nelle due zone (condizione iniziale)
#define P_L 100000.0
#define P_R 10000.0

#define H_NUMBER 32
//raggio di interazione per avere in media 4*H_NUMBER vicini in [-2h, +2h]
#define H (H_NUMBER * L_SYS/npoints)
//per proteggere evoluzione da effetti di bordo(vedi funzioni hydroforces)
#define L_BUFFER 4*H_NUMBER
#define R_BUFFER ((int)(4/8.0*H_NUMBER))

//cambio di unità temporale a 10**-3 s per avere valori
//numerici più vicini all'unità
//le pressioni solo le uniche condiz iniziali
//che ne risentono
#define T_UNIT 1.0e+3
#ifdef T_UNIT
#undef P_L
#define P_L (100000.0/T_UNIT/T_UNIT)
#undef P_R
#define P_R (10000.0/T_UNIT/T_UNIT)
#endif

//per poter cambiare tra float e double
typedef double MYFLOAT;

//definiti in params.txt
int nsteps, npoints;
MYFLOAT epsilon;

struct particle{
                int index;
                MYFLOAT pos;
                MYFLOAT vel;
                MYFLOAT acc;
                MYFLOAT en;
                MYFLOAT dendt;
                MYFLOAT mass;
                MYFLOAT density;
                MYFLOAT pressure;
                };
            
void gen_points(struct particle sistema[], int npunti);

void print_state(struct particle sistema[], char nome[]);
void clean_old_files();
void print_specs4graph(char nome[]);
FILE *open2write(char nome[]);

void kdk(struct particle sistema[], int npassi);
void kdk_w_plant(struct particle sistema[], int npassi);
void first_interactions(struct particle sistema[]);

#endif