#include "plant.h"
/*
MODULO CON LE FUNZIONALITà DELL'ALBERO BINARIO
*/

struct pnode *talloc(void)
{
    return (struct pnode *) malloc(sizeof(struct pnode));
}

/*
Molto importante creare un BST bilanciato se vogliamo che sia efficiente.
Se le particelle rimangono ordinate nell'array sistema secondo le posizioni
possiamo creare una struttura bilanciata facendo salti opportuni 
nell'array sistema.
A questo scopo useremo una funzione drift che corregga l'ordine 
delle particelle dopo averle fatte muovere. 
*/ 
struct pnode *make_balanced_plant(struct pnode *p, struct particle sistema[], int low, int high)
{
    if (low > high)
        return NULL;

    int mid = (low+high) / 2;
    p = talloc();
    p->part = &sistema[mid];
    p->left = p->right = NULL;

    p->left = make_balanced_plant(p->left, sistema, low, mid-1);
    p->right = make_balanced_plant(p->right, sistema, mid+1, high);
    return p;
}


void free_plant(struct pnode *root)
{
    struct pnode *temp_l, *temp_r;
    if( root != NULL)
    {
        temp_l = root->left;
        temp_r = root->right;
        free(root);
        free_plant(temp_l);
        free_plant(temp_r);
    }
}

void check_plant(struct pnode *root, int *l)
{
    if( root != NULL)
    {
        printf(" %i \n", root->part->index);
        *l += 1;
        check_plant(root->left, l);
        check_plant(root->right, l);
    }
}
//scorre l'albero contando nodi di particelle in raggio di interazione
//va chiamata con *l = 0 nel programma (tanto si conta anche la particella stessa)
void count_neighbours(struct pnode *p, int *l, MYFLOAT l_bound, MYFLOAT r_bound)
{

    //arrivati alla fine di un ramo
    if (p == NULL)
        return;

    if (p->part->pos < l_bound) //solo il ramo a destra potrà contenere vicini
        count_neighbours(p->right, l, l_bound, r_bound);

    else if (p->part->pos > r_bound) //solo ramo a sinistra potrà contenere vicini
        count_neighbours(p->left, l, l_bound, r_bound);
    else //ha trovato un vicino, inoltre entrambi i rami possono contenere vicini
    {
        *l += 1;
        count_neighbours(p->right, l, l_bound, r_bound);
        count_neighbours(p->left, l, l_bound, r_bound); 
    }

/* 
    // versione che non esclude rami
    //arrivati alla fine di un ramo
    if (p == NULL)
        return;
    if (p->part->pos >= l_bound && p->part->pos <= r_bound)
    {
        *l += 1;
        count_neighbours(p->right, l, l_bound, r_bound);
        count_neighbours(p->left,  l, l_bound, r_bound);
    }
    else
    {
        count_neighbours(p->right, l, l_bound, r_bound);
        count_neighbours(p->left,  l, l_bound, r_bound);
    }
*/
}


//riempie array di vicini la cui lunghezza è data da count_neighbours
//va chiamata con *l = 0 nel programma
void make_neighbours(struct pnode *p, int *l, 
                    struct particle *vicini[],
                    int pindex,
                    MYFLOAT l_bound,
                    MYFLOAT r_bound)

{
    //arrivati alla fine di un ramo
    if (p == NULL)
        return;
    /*
    //non vogliamo che una particella registri se stessa come vicino
    if (p->part->index == pindex)
    {
        make_neighbours(p->right, l, vicini, pindex, l_bound, r_bound);
        make_neighbours(p->left,  l, vicini, pindex, l_bound, r_bound); 
    }
    */
    else if (p->part->pos < l_bound) //solo il ramo a destra potrà contenere vicini
        make_neighbours(p->right, l, vicini, pindex, l_bound, r_bound);

    else if (p->part->pos > r_bound) //solo ramo a sinistra potrà contenere vicini
        make_neighbours(p->left,  l, vicini, pindex, l_bound, r_bound); 
    else //entrambi i rami possono contenere particelle vicine
    {
        vicini[*l] = (p->part);
        *l += 1;

        make_neighbours(p->right, l, vicini, pindex, l_bound, r_bound);
        make_neighbours(p->left,  l, vicini, pindex, l_bound, r_bound); 
    }

/*    // versione che non esclude rami
    //arrivati alla fine di un ramo
    if (p == NULL)
        return;
    //non vogliamo che una particella registri se stessa come vicino
    if (p->part->pos == pindex)
    {
        make_neighbours(p->right, l, vicini, pindex, l_bound, r_bound);
        make_neighbours(p->left,  l, vicini, pindex, l_bound, r_bound); 
    }
    else if (p->part->pos >= l_bound && p->part->pos <= r_bound)
    {
        vicini[*l] = *(p->part);
        *l += 1;
        make_neighbours(p->right, l, vicini, pindex, l_bound, r_bound);
        make_neighbours(p->left,  l, vicini, pindex, l_bound, r_bound)
    }
    else
    {
        make_neighbours(p->right, l, vicini, pindex, l_bound, r_bound);
        make_neighbours(p->left,  l, vicini, pindex, l_bound, r_bound)
    }
*/
}

