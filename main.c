#include "header.h"

//Programma di Nicolò Venuti
//Esame di programmazione avanzata per la fisica


//----------------------------------------------------------------------------------------
int main(int argc, char *argv[])
{   
	// Serve calcolare t di esecuzione in maniera diversa con openmp
    #ifndef _OPENMP
    clock_t begin = clock();
    #else
    double end, begin = omp_get_wtime();
    #endif

    clean_old_files();

    //legge parametri da file
    FILE *punt_f = fopen("params.txt", "r");
    if (punt_f == NULL)
    {
        printf("Error while opening params.txt");
        exit(EXIT_FAILURE);
    }
    fscanf(punt_f, "%le %i %i", &epsilon, &npoints, &nsteps);
    fclose(punt_f);

    //perciò che il programma ./outdir/grafico.py sappia
    //quanto lunghi sono i file di dati
    //e altre cose utili:
    print_specs4graph("./outdir/specs.py");
    
    //alloca array di strutture particelle
    struct particle *sistema = (struct particle *) malloc(sizeof(struct particle) * npoints);
    
    //generazione distribuzione iniziale
    gen_points(sistema, npoints);

    //quando serve fai:
    //print_state(sistema, "./outdir/initial.txt");
    //printf("scrivi qualcosa per continuare: ");
    //int c = getc(stdin);

    if(argc != 2)
    {
        printf("Programma si aspetta un argomento da riga di comando:\n"
                "1 per controllare il primo timestep \n"
                "2 per KDK\n"
                "3 per KDK con ricerca binaria dei vicini\n");
        exit(EXIT_FAILURE);
    }

        
    int condition = atoi(argv[1]);
    if (condition == 1)
    {
        printf("\nCalcolo accelerazioni al primo step\n");
        first_interactions(sistema);
    }
    else if (condition == 2)
    {
        printf("Integro con KDK\n");
        kdk(sistema, nsteps);
    }
    else if (condition == 3)
    {
        printf("Integro con KDK con albero binario\n");
        kdk_w_plant(sistema, nsteps);
    }
    else
    {
        printf("Argomento sbagliato, devi usare: \n"
                "1 per controllare il primo timestep \n"
                "2 per KDK\n"
                "3 per KDK con ricerca binaria dei vicini\n");
        exit(EXIT_FAILURE);
    }
    
    
    print_state(sistema, "./outdir/instant10.txt");
    
    free(sistema);
    
    #ifndef _OPENMP
    clock_t end = clock();
    MYFLOAT time_spent = (MYFLOAT)(end - begin) / CLOCKS_PER_SEC;
    printf("\nTempo di esecuzione: %f s\n", time_spent);
    #else
    end = omp_get_wtime();
    double time_spent = end - begin;
    printf("\nTempo di esecuzione parallelo: %f s\n", time_spent);
    #endif

    printf("Eseguire ./outdir/grafico.py per vedere i grafici \n");
}
//----------------------------------------------------------------------------------------
    
