#include "integrazione.h"
#include "plant.h"


//sequenza di evoluzione è ((( F(t) ))) K(dt/2) D(dt) F(t+dt) K(dt/2)
void kdk(struct particle sistema[], int npassi)
{
    //file dove stampi le varie istantanee
    int file_counter = 0;
    char filename[40];
    MYFLOAT dt = 0;

    //per l>0 avrai già calcolato le densità e energie attuali
    //alla fine del ciclo di operazioni precedente
    compute_density(sistema, npoints);
    compute_energy(sistema, npoints);
    compute_pressure(sistema, npoints);
    hydroforces(sistema);
    
    
    for(int l = 0; l < npassi; l++)
    {   
        dt = dt_adapt(sistema);
        //printf("dt = %e\n", dt);

        if ( (10*l) % npassi == 0)
        {   
            //visualizzazione della velocità di progresso dell'esecuzione
            //di decimi in decimi
            printf("%i/%i passi \n", l, npassi);
            #ifdef T_UNIT
            printf("dt = %e * %.1e s\n", dt, 1/T_UNIT);
            #else 
            printf("dt = %e s\n", dt);
            #endif

            sprintf(filename, "./outdir/instant%i.txt", file_counter);
            //generiamo dieci istantanee del sistema durante il corso dell'evoluzione
            print_state(sistema, filename);
            file_counter += 1;
        }
        

        //K(dt/2)
		kick(sistema, dt*0.5);
            
        //D(dt)
        drift(sistema, dt);
        
        //F(t+dt)
		compute_density(sistema, npoints);
        compute_pressure(sistema, npoints);
        hydroforces(sistema);

        //K(dt/2)
		kick(sistema, dt*0.5);

    }  
}

//sequenza di evoluzione è ((( F(t) ))) K(dt/2) D(dt) F(t+dt) K(dt/2)
void kdk_w_plant(struct particle sistema[], int npassi)
{
    //file dove stampi le varie istantanee
    int file_counter = 0;
    char filename[40];
    MYFLOAT dt = 0;

    //creo l'albero prima di calcolare densità e forze
    struct pnode *root = NULL;
    root = make_balanced_plant(root, sistema, 0, npoints-1);

    //per l>0 avrai già calcolato le densità e energie attuali
    //alla fine del ciclo di operazioni precedente
    bst_compute_density(sistema, root, npoints);
    compute_energy(sistema, npoints);
    compute_pressure(sistema, npoints);
    bst_hydroforces(sistema, root);
    free_plant(root);

    
    for(int l = 0; l < npassi; l++)
    {   
        dt = dt_adapt(sistema);
        //printf("dt = %e\n", dt);

        if ( (10*l) % npassi == 0)
        {   
            //visualizzazione della velocità di progresso dell'esecuzione
            //di decimi in decimi
            printf("%i/%i passi \n", l, npassi);
            #ifdef T_UNIT
            printf("dt = %e * %.1e s\n", dt, 1/T_UNIT);
            #else 
            printf("dt = %e s\n", dt);
            #endif
            sprintf(filename, "./outdir/instant%i.txt", file_counter);
            //generiamo dieci istantanee del sistema nel corso dell'evoluzione
            print_state(sistema, filename);
            file_counter += 1;
        }
        

        //K(dt/2)
        kick(sistema, dt*0.5);
            
        //D(dt)
        //protegge da inversioni di posizione delle particelle
        corrected_drift(sistema, dt);
        
        //dopo il drift e l'eventuale riordinamento, ricreo l'albero
        struct pnode *root = NULL;
        root = make_balanced_plant(root, sistema, 0, npoints-1);

        //F(t+dt)
        bst_compute_density(sistema, root, npoints);
        compute_pressure(sistema, npoints);
        bst_hydroforces(sistema, root);
        free_plant(root);

        //K(dt/2)
        kick(sistema, dt*0.5);

    }  
}


void kick(struct particle sistema[], MYFLOAT dt)
{    
#pragma omp parallel for

    for (int i = 0; i<npoints; i++)
    {
    	sistema[i].vel = sistema[i].vel + sistema[i].acc * dt;
    	sistema[i].en = sistema[i].en + sistema[i].dendt * dt;
    }
}

void drift(struct particle sistema[], MYFLOAT dt)
{
#pragma omp parallel for

    for (int i = 0; i<npoints; i++)
    {
        sistema[i].pos = sistema[i].pos + sistema[i].vel * dt;
        
        //semplici boundary conditions a specchio
        //non funzionano molto bene, non farci affidamento
        if (sistema[i].pos < 0)
            sistema[i].vel = fabs(sistema[i].vel);
        else if (sistema[i].pos > 2)
            sistema[i].vel = -1*fabs(sistema[i].vel);
    }
}

//protegge da inversioni di posizione delle particelle
void corrected_drift(struct particle sistema[], MYFLOAT dt)
{
#pragma omp parallel for

    for (int i = 0; i<npoints; i++)
    {
        sistema[i].pos = sistema[i].pos + sistema[i].vel * dt;
        
        //semplici boundary conditions a specchio
        //non funzionano molto bene, non farci affidamento
        if (sistema[i].pos < 0)
            sistema[i].vel = fabs(sistema[i].vel);
        else if (sistema[i].pos > 2)
            sistema[i].vel = -1*fabs(sistema[i].vel);
    }
    //protegge da inversioni di posizione delle particelle
    struct particle ptemp;
    for (int i = 1; i<npoints; i++)
    {
        while (sistema[i].pos < sistema[i-1].pos)
        {
            ptemp = sistema[i];
            sistema[i] = sistema[i-1];
            sistema[i-1] = ptemp;

        }
    }
}
