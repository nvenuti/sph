#include "header.h"

//da usare se serve eseguire una qualche versione alternativa/ridotta del programma

//----------------------------------------------------------------------------------------
int main(int argc, char *argv[])
{   
    clock_t begin = clock();

    //perciò che il programma grafico.py sappia
    //quanto lunghi sono i file di dati
    //e altre cose utili:
    print_specs4graph("./outdir/specs.py");
    
    //alloca array di strutture particelle
    struct particle *sistema = (struct particle *) malloc(sizeof(struct particle) * npoints);
    
    //generazione distribuzione iniziale
    gen_points(sistema, npoints);

    //quando serve fai:
    //print_state(sistema, "./outdir/initial.txt");
    //printf("scrivi qualcosa per continuare: ");
    //int c = getc(stdin);
        
    int condition = atoi(argv[1]);
    if (condition == 1)
    {
        printf("\nCalcolo accelerazioni al primo step\n");
        first_interactions(sistema);
    }
    else if (condition == 2)
    {
        printf("integro con KDK\n");
        kdk(sistema, nsteps);
    }
    else
    {
        printf("Bad condition, devi usare: \n"
                "1 per controllare il primo timestep \n"
                "2 per kdk\n");
        exit(EXIT_FAILURE);
    }
    
    
    print_state(sistema, "./outdir/final.txt");
    
    free(sistema);
    
    clock_t end = clock();
    double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("tempo di esecuzione: %f s\n", time_spent);
}
//----------------------------------------------------------------------------------------
