#include "header.h"


//scrive su file tutte le informazioni sullo stato del sistema
void print_state(struct particle sistema[], char nome[])
{
    FILE *punt_f = open2write(nome);
   
    for (int j = 0; j < npoints; j++)
    {   
        //versione lungaaaaaaa
        
        fprintf(punt_f, "%i %e %e %e %e %e %e \n", j,
                                            sistema[j].pos,
                                            sistema[j].vel,
                                            sistema[j].acc,
                                            sistema[j].en,
                                            sistema[j].density,
                                            sistema[j].pressure
                 );
        
        
        //versione ecologica, ci metti dentro solo quello che ti serve al momento
        //#savetheplanet
        //#natureisprecious
        /*
        fprintf(punt_f, "%e %e\n", sistema[j].pos,
        	);
        */
    }    
    
    fclose(punt_f) ;   

}


//non vogliamo che rimangano in giro istantanee del sistema di esecuzioni precedenti
void clean_old_files()
{
    char filename [40];
    FILE *f;

    for(int i = 0; i <= 10; i++)
    {
        sprintf(filename, "./outdir/instant%i.txt", i);

        f = fopen(filename, "r");
        if (f == NULL)
            continue;  

        int r = remove(filename);
        if (r == -1)
        {
            printf("Errore nel ripulire i file di output\n");
            exit(EXIT_FAILURE);
        }
    }    
}


void print_specs4graph(char nome[])
{
    FILE *graph_specs = open2write(nome);
    
    fprintf(graph_specs, "npoints = %i\n", npoints);
    fprintf(graph_specs, "nsteps = %i\n", nsteps);
    fprintf(graph_specs, "L_SYS = %f\n", L_SYS);
    fprintf(graph_specs, "H = %f\n", H);
    fprintf(graph_specs, "L_BUFFER = %i\n", L_BUFFER);
    fprintf(graph_specs, "R_BUFFER = %i\n", R_BUFFER);




     
    fclose(graph_specs);
}

//Tanto usi sempre questa routine...
FILE *open2write(char nome[])
{
    int resullt = mkdir("./outdir/")
    FILE *punt_f = fopen(nome, "w");
    if (punt_f == NULL)
    {
        printf("Error while opening file %s \n", nome);
        exit(EXIT_FAILURE);
    }
    return punt_f;
}