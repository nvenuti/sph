#include <stdio.h>
#include <stdlib.h>

#define frand() ((double) rand() / (RAND_MAX+1.0))

void open2write(FILE *punt_f, char nome[]);

//per controllare di generare numeri casuali nell'intervallo
//[0, 99]  nella maniera giusta

int main()
{
	FILE *punt_f;
	char nome[50] = "./outdir/dat.txt";
	open2write(punt_f, nome);

	for (int i = 0; i < 10; i++)
	{
		int n = frand() * (100);
	    fprintf(punt_f, "%i    %i \n", i, n);
	}
}

void open2write(FILE *punt_f, char nome[])
{
	printf("%s", nome);
	getchar();
    punt_f = fopen(nome, "w");
    if (punt_f == NULL)
    {
        printf("Error while opening file %s \n", nome);
        exit(EXIT_FAILURE);
    }
}