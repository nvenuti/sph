#include "integrazione.h"
#include "plant.h"


//spline kernel per 1D
MYFLOAT W(MYFLOAT r)
{
    MYFLOAT q = fabs(r / H);
    if(q <= 1.)
    {
        return (1 - 1.5*q*q + 0.75*q*q*q)*2/(3*H);
    }
    else if(q > 1. && q <= 2.)
    {
        return 0.25*(2 - q)*(2 - q)*(2 - q)*2/(3*H);
    }
    else
    {
        return 0;
    }
}



//definisco direttamente la derivata di W
//il valore asoluto in W obbliga a differenziare in 4 casi
MYFLOAT DW(MYFLOAT r)
{
    MYFLOAT q = (r / H);
    if(q >= -2. && q < -1.)
        return +0.5*(2 + q)*(2 + q)/(H*H);

    else if(q >= -1. && q < 0)
        return (-2*q - 1.5*q*q)/(H*H);

    else if(q >= 0 && q < 1.)
        return (-2*q + 1.5*q*q)/(H*H);

    else if(q >= 1. && q <= 2.)
        return -0.5*(2 - q)*(2 - q)/(H*H);
    else
        return 0;
}



//densità associata ad ogni particella
void compute_density(struct particle sistema[], int npunti)
{    
//istruzione per esecuzione in parallelo
#pragma omp parallel for

    for(int i = 0; i < npunti; i++)
    {
        sistema[i].density = 0.0;
        for(int j = 0; j < npunti; j++)
        {
        //    if(j == i)
        //        continue;

            MYFLOAT dist = sistema[i].pos - sistema[j].pos;
            //saltiamo subito i casi dove non si ha interazione
            if ( fabs(dist) > 2.0*H)
                continue;

            sistema[i].density = sistema[i].density +  sistema[j].mass * W(dist);
            /*
            //controllo valori
            MYFLOAT val = W(dist);
            printf("\ndensity step %i-%i :\n"
                    "dist = %e\n"
                    "q = %e\n"
                    "W = %e\n"
                    "sistema[i].density = %e\n",
                    i, j, dist, dist/H, val, sistema[i].density);
            getchar();
            */
        }
    }
}

//ricerca dei vicini con l'albero binario come suggerito nella consegna
void bst_compute_density(struct particle sistema[], struct pnode *root, int npunti)
{
//istruzione per esecuzione in parallelo
#pragma omp parallel for

    for(int i = 0; i < npunti; i++)
    {
        sistema[i].density = 0.0;
        int count = 0;
        count_neighbours(root, &count, sistema[i].pos-2*H, sistema[i].pos+2*H);

        struct particle **vicini = (struct particle **) malloc(sizeof(struct particle *) * count);
        count = 0;
        make_neighbours(root, &count, vicini, sistema[i].index,
                                                sistema[i].pos-2*H, 
                                                sistema[i].pos+2*H);
        for(int j = 0; j < count; j++)
        {
            MYFLOAT dist = sistema[i].pos - vicini[j]->pos;
            sistema[i].density = sistema[i].density + vicini[j]->mass * W(dist);
            //vedi controllo valori se serve
        }
        free(vicini);
    }
}

void compute_pressure(struct particle sistema[], int npunti)
{
    for(int i = 0; i < npunti; i++)
    {
        sistema[i].pressure = (GAMMA - 1) * sistema[i].density * sistema[i].en;
    }
}

void compute_energy(struct particle sistema[], int npunti)
{
    for(int i = 0; i < npunti; i++)
    {
        sistema[i].en = sistema[i].pressure / (GAMMA - 1) / sistema[i].density;
    }
}



void hydroforces(struct particle sistema[])
{
    /*
    Condizioni al contorno:
    Escludendo il giusto numero di particelle dall'evoluzione si mantengono
    due zone cuscinetto larghe 4*H in testa e in coda al tubo da usare
    come condizioni al contorno
    NNB soluzione valida solo fino a che lo shock non raggiunge
    le estremità del tubo!!!
    */

//istruzione per esecuzione in parallelo
#pragma omp parallel for

    for (int i = L_BUFFER; i < (npoints-R_BUFFER); i++)
    {
        sistema[i].acc = 0.;
        sistema[i].dendt = 0.;

        for (int j = 0; j<npoints; j++)
        {
        //    if(j == i)
        //        continue;

            MYFLOAT dist = sistema[i].pos - sistema[j].pos;
            //saltiamo subito i casi dove non si ha interazione
            if ( fabs(dist) > 2.0*H)
                continue;

            MYFLOAT P_rho_i = sistema[i].pressure / sistema[i].density / sistema[i].density;
            MYFLOAT P_rho_j = sistema[j].pressure / sistema[j].density / sistema[j].density;
            
            sistema[i].acc = sistema[i].acc 
                            - sistema[j].mass 
                            * ( P_rho_i + P_rho_j + P_ij(&sistema[i], &sistema[j]) )
                            * DW(dist);

            sistema[i].dendt = sistema[i].dendt
                            + sistema[j].mass 
                            * ( P_rho_i + 0.5 * P_ij(&sistema[i], &sistema[j]) ) 
                            * (sistema[i].vel - sistema[j].vel)
                            * DW(dist);
            //controllo valori
            /*
            MYFLOAT val = DW(dist);
            MYFLOAT val2 = P_ij(&sistema[i], &sistema[j]);
            printf("\nforces step %i-%i :\n"
                    "dist = %e\n"
                    "q = %e\n"
                    "DW = %e\n"
                    "P_rho_i = %e\n"
                    "P_rho_j = %e\n"
                    "P_ij = %e\n"
                    "sistema[i].acc = %e\n"
                    "sistema[i].dendt = %e\n",
                    i, j, dist, dist/H, val, P_rho_i, P_rho_j, val2, sistema[i].acc, sistema[i].dendt);
            getchar();
            */

        }
    }
}

//ricerca dei vicini con l'albero binario come suggerito nella consegna
void bst_hydroforces(struct particle sistema[], struct pnode *root)
{
    /*
    Condizioni al contorno:
    Escludendo il giusto numero di particelle dall'evoluzione si mantengono
    due zone cuscinetto larghe 4*H in testa e in coda al tubo da usare
    come condizioni al contorno
    NNB soluzione valida solo fino a che lo shock non raggiunge
    le estremità del tubo!!!
    */

//istruzione per esecuzione in parallelo
#pragma omp parallel for

    for(int i = L_BUFFER; i < (npoints-R_BUFFER); i++)
    {
        sistema[i].acc = 0.0;
        sistema[i].dendt = 0.0;
        int count = 0;
        count_neighbours(root, &count, sistema[i].pos-2*H, sistema[i].pos+2*H);

        struct particle **vicini = (struct particle **) malloc(sizeof(struct particle *) * count);
        count = 0;
        make_neighbours(root, &count, vicini, sistema[i].index,
                                                sistema[i].pos-2*H, 
                                                sistema[i].pos+2*H);
        for(int j = 0; j < count; j++)
        {
            MYFLOAT dist = sistema[i].pos - vicini[j]->pos;

            MYFLOAT P_rho_i = sistema[i].pressure / sistema[i].density / sistema[i].density;
            MYFLOAT P_rho_j = vicini[j]->pressure / vicini[j]->density / vicini[j]->density;
            
            sistema[i].acc = sistema[i].acc 
                            - vicini[j]->mass 
                            * ( P_rho_i + P_rho_j + P_ij(&sistema[i], vicini[j]) )
                            * DW(dist);

            sistema[i].dendt = sistema[i].dendt
                            + vicini[j]->mass 
                            * ( P_rho_i + 0.5 * P_ij(&sistema[i], vicini[j]) ) 
                            * (sistema[i].vel - vicini[j]->vel)
                            * DW(dist);
            //vedi controllo valori se serve
        }
        free(vicini);
    }
}


MYFLOAT P_ij(struct particle *corpo_i, struct particle *corpo_j)
{   
    MYFLOAT xij = corpo_i->pos - corpo_j->pos;
    MYFLOAT vij = corpo_i->vel - corpo_j->vel;
    if (xij*vij < 0)
    {
        MYFLOAT mu = mu_ij(xij, vij);
        return  ( -ALPHA * (c_s(corpo_i) + c_s(corpo_j))*0.5 * mu + BETA * mu*mu )
                / ((corpo_i->density + corpo_j->density)*0.5);
    }
    else
    {
        return 0;
    }
}


MYFLOAT mu_ij(MYFLOAT xij, MYFLOAT vij)
{
    return (H * xij * vij) / (xij*xij + epsilon*H*H);
}


MYFLOAT dt_adapt(struct particle sistema[])
{
    //cerco dt minimo con criterio di Courant
    //per fare meno conti non uso 1/c_s (che ha sqrt)
    //ma il suo quadrato (rapporto rho/P)
    MYFLOAT min = sistema[0].density / sistema[0].pressure;
    for(int i = L_BUFFER; i < (npoints-R_BUFFER); i++)
    { 
        MYFLOAT val = sistema[i].density / sistema[i].pressure;
        if (val < min)
        {
            min = val;
        }
    }
    //questo qui è dt
    return K * H * sqrt(min / GAMMA);
}

MYFLOAT c_s(struct particle *corpo)
{
    return sqrt(GAMMA * corpo->pressure / corpo->density);
}