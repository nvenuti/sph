#include "integrazione.h"
/*
essenzialmente è il primo step di integrazione,
fatto semplice semplice, per verificare che le funzioni 
funzioninno in maniera sensata
*/

void first_interactions(struct particle sistema[])
{    
    //F(t)
    compute_density(sistema, npoints);
    compute_energy(sistema, npoints);
    hydroforces(sistema);

    print_state(sistema, "./outdir/initial.txt");
    //printf("p\n");
    //getchar();

    MYFLOAT dt = dt_adapt(sistema);
    printf("dt = %e\n", dt);
 
    //K(dt/2)
    kick(sistema, dt*0.5);
        
    //D(dt)
    drift(sistema, dt);
    
    //F(t+dt)
    //accelerazione con le nuove posizioni
    compute_density(sistema, npoints);
    compute_energy(sistema, npoints);
    hydroforces(sistema);

    //K(dt/2)
    kick(sistema, dt*0.5);
/*
    printf("Use some input to continue every line\n");

    for (int i = 0; i < npoints; i++)
    {
        printf("accelerazione (%i) : %e\n", i, sistema[i].acc);
        printf("energia (%i) : %e\n", i, sistema[i].en);
        getchar();
    }
*/
}