import numpy as np
import matplotlib.pyplot as plt
from matplotlib import gridspec
import sys
import specs
import importlib
importlib.reload(specs)

#fai attenzione a non delegare troppo l'elaborazione dei dati a questo file
#quando gli stampi perdi precisione

#di seguito vengono stampati i grafici
#delle proprietà delle particelle al tempo richiesto


if(len(sys.argv) != 2):
    sys.exit('''Eseguire con argomento da linea di comando da 0 a 10  
            per stampare stato del sistema al tempo desiderato''')


l_bound = specs.L_BUFFER*specs.L_SYS/specs.npoints
r_bound = 8*specs.R_BUFFER*specs.L_SYS/specs.npoints

num = sys.argv[1]
n = int(num)

data = np.loadtxt('instant'+num+'.txt')

x = data[:, 1]

fig = plt.figure(figsize=(18, 9.61))
plt.subplots_adjust(left=0.06, bottom=0.06, right=0.97, top=0.93, wspace=0, hspace=0)
#fig.tight_layout(pad=0)

spec1 = gridspec.GridSpec(ncols=3, nrows=2,
                         wspace=0.12, hspace=0.2)
spec2 = gridspec.GridSpec(ncols=2, nrows=2,
                         wspace=0.1, hspace=0.2)

#velocità
y = data[:, 2]
ax5 = fig.add_subplot(spec1[0])
ax5.clear() 
ax5.set_title('Velocità')    
ax5.set_xlabel('x')
ax5.plot(x, y, marker='o', ms=1.2, lw=0)
ax5.axvline(x=0, lw=1, c='black')
ax5.axvline(x=2, lw=1, c='black', label='Limite del tubo')
ax5.axvline(x=l_bound, lw=0.5, label='Zone cuscinetto')
ax5.axvline(x=2 - r_bound, lw=0.5)


#accelerazioni
y = data[:, 3]
ax1 = fig.add_subplot(spec1[1])
ax1.clear() 
ax1.set_title('Accelerazioni')    
ax1.set_xlabel('x')
ax1.plot(x, y, marker='o', ms=1.2, lw=0)
ax1.axvline(x=0, lw=1, c='black')
ax1.axvline(x=2, lw=1, c='black')
ax1.axvline(x=l_bound, lw=0.5)
ax1.axvline(x=2 - r_bound, lw=0.5)


#energia interna
y = data[:, 4]
ax4 = fig.add_subplot(spec1[2])
ax4.clear() 
ax4.set_title('Energie')    
ax4.set_xlabel('x')
ax4.plot(x, y, marker='o', ms=1.2, lw=0)
ax4.axvline(x=0, lw=1, c='black')
ax4.axvline(x=2, lw=1, c='black')
ax4.axvline(x=l_bound, lw=0.5)
ax4.axvline(x=2 - r_bound, lw=0.5)


#densità
y = data[:, 5]
ax2 = fig.add_subplot(spec2[2])
ax2.clear() 
ax2.set_title('Densità')    
ax2.set_xlabel('x')
ax2.plot(x, y, marker='o', ms=1.2, lw=0)
ax2.axvline(x=0, lw=1, c='black')
ax2.axvline(x=2, lw=1, c='black')
ax2.axvline(x=l_bound, lw=0.5)
ax2.axvline(x=2 - r_bound, lw=0.5)


#pressione
y = data[:, 6]
ax3 = fig.add_subplot(spec2[3])
ax3.clear() 
ax3.set_title('Pressione')    
ax3.set_xlabel('x')
ax3.plot(x, y, marker='o', ms=1.2, lw=0)
ax3.axvline(x=0, lw=1, c='black')
ax3.axvline(x=2, lw=1, c='black')
ax3.axvline(x=l_bound, lw=0.5)
ax3.axvline(x=2 - r_bound, lw=0.5)


fig.suptitle("N. particelle ="+str(specs.npoints)+
            "    N. passi = "+ str(int(specs.nsteps*n/10)) )
fig.legend()
fig.show()


#conservazione energia interna
e_glob = np.zeros(11)
diffs = np.zeros(10)
t = np.linspace(1, 10, 10)
for i in range(0, 11):
    data = np.loadtxt('instant'+str(i)+'.txt', usecols=[4])
    e_glob[i] = np.sum(data)
    if i>0:
        diffs[i-1] = (e_glob[i]-e_glob[i-1]) / ( (e_glob[i]+e_glob[i-1])/2 )

fig = plt.figure(figsize=(8, 5))
ax1 = fig.add_subplot()
ax1.plot(t, diffs, marker='o', ms=1.2, lw=1)
ax1.set_title('Conservazione energia interna')    
ax1.set_xlabel('istanti')
ax1.set_ylabel('$\Delta u_{tot} \, / \, u_{tot}$')
ax1.set_xticks(t)
fig.show()

#OBSOLETO
#--------------------------------------------------------------------------------
'''
#metti True per stampare anche
#le condizioni iniziali:
a = False
if(a == True):
    x = data[0][:, 1]

    #accelerazioni
    y = data[0][:, 3]
    
    fig = plt.figure(figsize=(15, 10))
    ax1 = fig.add_subplot(111)
    ax1.set_title('accelerazioni iniziali')    
    ax1.set_xlabel('x')
    ax1.plot(x, y, marker='o', ms=1.2, lw=0)
    fig.show()



    #densità
    y = data[0][:, 5]

    fig = plt.figure(figsize=(15, 10))
    ax2 = fig.add_subplot(111)
    ax2.set_title('densità iniziali')    
    ax2.set_xlabel('x')
    ax2.plot(x, y, marker='o', ms=1.2, lw=0)
    fig.show()


    #pressione
    y = data[0][:, 6]

    fig = plt.figure(figsize=(15, 10))
    ax3 = fig.add_subplot(111)
    ax3.set_title('pressione iniziali')    
    ax3.set_xlabel('x')
    ax3.plot(x, y, marker='o', ms=1.2, lw=0)
    fig.show()



    #energia interna
    y = data[0][:, 4]

    fig = plt.figure(figsize=(15, 10))
    ax4 = fig.add_subplot(111)
    ax4.set_title('energie iniziali')    
    ax4.set_xlabel('x')
    ax4.plot(x, y, marker='o', ms=1.2, lw=0)
    fig.show()
#--------------------------------------------------------------------------------
'''