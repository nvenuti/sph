#ifndef planth
#define planth
#include "header.h"


struct pnode 
{
    struct particle *part;
    struct pnode *left;
    struct pnode *right;
};


struct pnode *talloc(void);

struct pnode *make_balanced_plant(struct pnode *root, struct particle sistema[], int low, int high);
void free_plant(struct pnode *root);
void check_plant(struct pnode *root, int *l);


void count_neighbours(struct pnode *p, int *l, MYFLOAT l_bound, MYFLOAT r_bound);
void make_neighbours(struct pnode *p, int *l, 
                    struct particle *vicini[],
                    int pindex,
                    MYFLOAT l_bound,
                    MYFLOAT r_bound);

//void ordered_treeprint(struct pnode *p, struct particle ordered[], int *l)
//struct pnode *addnode(struct pnode *p, struct particle *newpart);

#endif