#ifndef integrazioneh
#define integrazioneh
#include "header.h"
#include "plant.h"


#define ALPHA 1.0
#define BETA 2.0
#define GAMMA (5./3.)
#define K 0.1 //0.15


//DICHIARAZIONE DELLE FUNZIONI USATE IN integrazione.c

void compute_density(struct particle sistema[], int npunti);
void bst_compute_density(struct particle sistema[], struct pnode *root, int npunti);

void compute_pressure(struct particle sistema[], int npunti);
void compute_energy(struct particle sistema[], int npunti);

MYFLOAT W(MYFLOAT r);
MYFLOAT DW(MYFLOAT r);

MYFLOAT P_ij(struct particle *corpo_i, struct particle *corpo_j);
MYFLOAT mu_ij(MYFLOAT xij, MYFLOAT vij);
MYFLOAT dt_adapt(struct particle sistema[]);
MYFLOAT c_s(struct particle *corpo);

void hydroforces(struct particle sistema[]);
void bst_hydroforces(struct particle sistema[], struct pnode *root);

void drift(struct particle sistema[], MYFLOAT dt);
void corrected_drift(struct particle sistema[], MYFLOAT dt);
void kick(struct particle sistema[], MYFLOAT dt);

#endif