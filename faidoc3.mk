#Headerfile modificato da versione .3 di gMurante

#NB!!!!!
#NEI MAKEFILE DEVI USARE TAB, NON VANNO BENE 4 SPAZI
#NB!!!!!

#comando per compilazione è:
#make -f faidoc.3

#'yes' può essere utile per preparare già un piccolo input
#yes '1' | ./doit
#NB non puoi usare yes e getchar() bene insieme!

#NNB ^^^ meglio usare argc ed argv nel main per gestire argomenti da stdin


EXEC = doit

CC = gcc
CFLAGS = -g -fno-omit-frame-pointer #-fopenmp

LIBS = m
LIBDIR = /usr/lib/

HEADERS = header.h integrazione.h plant.h
OBJS = main.o initialcond.o integrazione.o print_methods.o sph_funcs.o first_interval.o plant.o

%.o: %.c $(HEADERS) faidoc3.mk
	$(CC) $(CFLAGS) -c $< -o $@

$(EXEC): $(OBJS) faidoc3.mk
	${CC} $(CFLAGS) -o $(EXEC) $(OBJS) -L$(LIBDIR) -l$(LIBS)

clean:
	@rm -f $(OBJS) $(EXEC) *~make
