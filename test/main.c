#include <stdio.h>
#include <stdlib.h>
#include <math.h>

//per controllare che W e DW abbiano la giusta espressione
//e abbiano l'andamento giusto


#define H (32.*2./2250)

double DW(double r);
double W(double r);

int main()
{
    for(int i = 0; i < 40; i++)
    {
        double x = H*0.06*i;
        double r = W(x);
        double d = DW(x);
        printf("%e    %e    %e    %e\n", x/H, x, r, d);
    }
}


double W(double r)
{
    double q = fabs(r / H);
    if(q < 1.)
    {
        return (1 - 1.5*q*q + 0.75*q*q*q)*2/(3*H);
    }
    else if(q > 1. && q < 2.)
    {
        return 0.25*(2 - q)*(2 - q)*(2 - q)*2/(3*H);
    }
    else
    {
        return 0;
    }
}

double DW(double r)
{
    double q = (r / H);
    if(q > -2. && q < -1.)
        return -0.5*(2 + q)*(2 + q)/(H*H);
    else if(q > -1. && q < 0)
        return (-2*q - 1.5*q*q)/(H*H);
    else if(q > 0 && q < 1.)
        return (-2*q + 1.5*q*q)/(H*H);
    else if(q > 1. && q < 2.)
        return -0.5*(2 - q)*(2 - q)/(H*H);
    else
        return 0;
}