#include "header.h"

void gen_points(struct particle sistema[], int npunti)
{
	//ripartizione dei punti tra le due zone
	int npoints_r = npunti * RHO_R / (RHO_L + RHO_R);
	int npoints_l = npunti - npoints_r;
	printf("In lz hai %i particelle \nIn rz hai %i particelle \n\n", npoints_l, npoints_r);

	//particelle in lz
	for(int i = 0; i < npoints_l; i++)
	{
		sistema[i].index = i;
		sistema[i].pos = 0.5 * L_SYS * ( (1.*i) / npoints_l );

	    sistema[i].vel = 0.0;
	    sistema[i].acc = 0.0;
	    sistema[i].en = 0.0;

	    sistema[i].pressure = P_L;
    
    	sistema[i].mass = M_SYS/npunti;
    }

	//particelle in RZ
	for(int i = npoints_l; i < npunti; i++)
	{
		sistema[i].index = i;
		sistema[i].pos = 0.5 * L_SYS * ( (1.*(i-npoints_l) / npoints_r ) + 1);

	    sistema[i].vel = 0.0;
	    sistema[i].acc = 0.0;
	    sistema[i].en = 0.0;

	    sistema[i].pressure = P_R;
    
    	sistema[i].mass = M_SYS/npunti;

    }

}
